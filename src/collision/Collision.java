package collision;

import entities.StaticObject.StaticObject;

import java.util.ArrayList;
import java.util.List;

public abstract class Collision {
    protected List<StaticObject> obstacles = new ArrayList<>();

    public void addObstacle(StaticObject obj) {
        obstacles.add(obj);
    }

    public void clear() {
        obstacles.clear();
    }
}
