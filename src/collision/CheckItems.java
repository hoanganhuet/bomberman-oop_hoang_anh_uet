package collision;

import entities.Entity;
import entities.StaticObject.Items;

import java.util.List;

public class CheckItems extends Collision {
    public void createCheckItems(List<Entity> staticObjs) {
        for (Entity i : staticObjs) {
            if (i instanceof Items) {
                Items obj = (Items) i;
                this.obstacles.add(obj);
            }
        }
    }
}
