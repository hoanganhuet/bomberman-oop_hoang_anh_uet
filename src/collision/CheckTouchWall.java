package collision;

import entities.DynamicObject.DynamicObject;
import entities.Entity;
import entities.StaticObject.Brick;
import entities.StaticObject.Portal;
import entities.StaticObject.StaticObject;
import entities.StaticObject.Wall;
import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.util.List;

public class CheckTouchWall extends Collision {
    public void createCheckTouchWall(List<Entity> staticObjs) {
        for (Entity i : staticObjs  ) {
            if (i instanceof Wall) {
                Wall obj = (Wall) i;
                this.obstacles.add(obj);
            } else if (i instanceof Brick) {
                Brick obj = (Brick) i;
                this.obstacles.add(obj);
            } else if (i instanceof Portal) {
                Portal obj = (Portal) i;
                this.obstacles.add(obj);
            }
        }
    }

    public boolean Touch(DynamicObject character) throws IOException {
        Rectangle characterShape = character.getCollisionShape();
        for (int i = 0; i < obstacles.size(); i++) {
            StaticObject obstacle = obstacles.get(i);
            Rectangle obstacleShape = obstacle.getCollisionShape();
            if (obstacleShape.getBoundsInParent().intersects(characterShape.getBoundsInParent())) {

                return true;
            }
        }
        return false;
    }
}
