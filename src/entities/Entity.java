package entities;

import graphic.Sprite;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;


public abstract class Entity {
    //Tọa độ X tính từ góc trái trên trong Canvas
    protected double x;

    //Tọa độ Y tính từ góc trái trên trong Canvas
    protected double y;

    protected Image img;

    public static List<Entity> stillObjects = new ArrayList<>();
    public static List<Entity> entities = new ArrayList<>();
    //public static List<Flame> listFlame = new ArrayList<>();
    //public static List<FlameSegment> listFlameSegment = new ArrayList<>();
    public static List<String> map = new ArrayList<>();
    public boolean _removed = false;
    public boolean _alive = true;
    public static int countEnemy = 0;

    //Khởi tạo đối tượng, chuyển từ tọa độ đơn vị sang tọa độ trong canvas
    public Entity( double x, double y, Image img) {
        this.x = x;
        this.y = y;
        this.img = img;

    }

    public void render(GraphicsContext gc) {
        gc.drawImage(img, x * Sprite.SCALED_SIZE, y * Sprite.SCALED_SIZE);
    }

    public abstract void update();

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setImage(Image img) {
        this.img = img;
    }
}
