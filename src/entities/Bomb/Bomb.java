package entities.Bomb;

import entities.Entity;
import javafx.scene.image.Image;

public class Bomb extends Entity {
    protected double _timeToExplode = 120;
    public int _timeAfter = 20;
    protected boolean _exploded = false;
    protected Flame[] _flames;
    public Bomb(int x, int y, Image img) {
        super( x, y, img);
    }

    @Override
    public void update() {

    }


}
