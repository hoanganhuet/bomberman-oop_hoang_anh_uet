package entities.Bomb;

import javafx.scene.image.Image;

public class PowerUpBomb extends Bomb {
    public PowerUpBomb(int x, int y, Image img) {
        super(x, y, img);
    }
}
