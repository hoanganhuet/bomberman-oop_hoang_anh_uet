package entities.DynamicObject;

import graphic.Sprite;
import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;
import javafx.scene.shape.Rectangle;

public abstract class Enemy extends DynamicObject {
    protected int timeToDead = 0;

    public Enemy(double x, double y, Image img) {
        super(x, y, img);

    }

    public Enemy(double x, double y, Image img, double vel) {
        super(x, y, img);
        this.vel = vel;
    }

    @Override
    public void update() {
        if (!_alive) timeToDead++;
        else {
            //checkDistanceToFlame();
            animate();
            calculateMove();
        }

    }

    public void calculateMove() {
        double xa = 0, ya = 0;

        if (_direction == "up") ya--;
        if (_direction == "down") ya++;
        if (_direction == "left") xa--;
        if (_direction == "right") xa++;

        if (canMove(_direction)) {
            x += xa * vel;
            y += ya * vel;
            if (canTurn() > 2 && round1(x) == Math.round(x) && round1(y) == Math.round(y)) {
                _direction = calculateDirection();
            }
        } else {
            _direction = calculateDirection() ;
        }
    }

    public int canTurn() {
        int count = 0;
        String direct = "";
        for (int i = 0; i < 4 ; i++) {
            if (i == 0) {
                direct = "up";
            } else if (i == 1) {
                direct = "right";
            } else if (i == 2) {
                direct = "down";
            } else {
                direct = "left";
            }
            if(canMove(direct)) {
                count++;
            }
        }
        return count;
    }



    protected abstract void chooseSprite();
    public abstract String calculateDirection();
}
