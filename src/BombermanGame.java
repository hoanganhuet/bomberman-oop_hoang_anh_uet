
import entities.DynamicObject.Balloom;
import entities.DynamicObject.Bomber;
import entities.DynamicObject.Oneal;
import entities.Entity;
import entities.StaticObject.Brick;
import entities.StaticObject.Grass;
import entities.StaticObject.Portal;
import entities.StaticObject.Wall;
import graphic.Sprite;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static entities.Entity.entities;
import static entities.Entity.stillObjects;

public class BombermanGame extends Application {

    public static final int WIDTH = 31;
    public static final int HEIGHT = 13;

    private GraphicsContext gc;
    private Canvas canvas;
    private Group root;
    private Bomber bomberman;


    public static void main(String[] args) {
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) throws IOException {
        // Tao Canvas
        canvas = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        gc = canvas.getGraphicsContext2D();

        // Tao root container
        root = new Group();
        root.getChildren().add(canvas);

        // Tao scene
        Scene scene = new Scene(root);

        // Them scene vao stage
        stage.setScene(scene);
        stage.setTitle("Bomberman Game");
        try {
            URL url = Paths.get("res/sprites/bomber_down.png").toUri().toURL();
            Image icon = new Image(String.valueOf(url));
            stage.getIcons().add(icon);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        stage.show();

        bomberman = new Bomber(1, 1, Sprite.player_right.getFxImage());
        bomberman.render(gc);

        scene.setOnKeyPressed(bomberman.keyPressed);
        scene.setOnKeyReleased(bomberman.keyReleased);

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                render();
                update();

            }
        };
        timer.start();
        createMap();
        entities.add(bomberman);

    }

    public void createMap() throws IOException {
        String url = "res/levels/Level1.txt";
        File fileMap = new File(String.valueOf(url));
        Scanner scanner = null;
        try {
        scanner = new Scanner(fileMap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = scanner.nextLine();
        String parameter[] = line.split(" ");
        for (int i = 0; i < HEIGHT; i++) {
            line = scanner.nextLine().trim() + "\n";
            Entity.map.add(line);
            //System.out.println(line);
        }
        Entity object;
        for (int i = 0; i < WIDTH; i++)  {
            for (int j = 0; j < HEIGHT; j++) {
                if (j == 0 || j == HEIGHT - 1 || i == 0 || i == WIDTH - 1) {
                    object = new Wall(i, j, Sprite.wall.getFxImage());
                    stillObjects.add(object);
                } else {
                    switch (Entity.map.get(j).charAt(i)) {
                        case '*': case 'f' : case 's' : case 'b' :
                            object = new Brick(i, j, Sprite.brick.getFxImage());
                            break;
                        case '#':
                            object = new Wall(i, j, Sprite.wall.getFxImage());
                            break;
                        case 'x':
                            object = new Portal(i, j, Sprite.portal.getFxImage());
                            stillObjects.add(object);
                            object = new Brick(i, j, Sprite.brick.getFxImage());
                            break;
                        case '1':
                            object = new Grass(i, j, Sprite.grass.getFxImage());
                            entities.add(new Balloom(i, j, Sprite.balloom_left1.getFxImage()));
                            Entity.countEnemy ++;
                            break;
                        case '2':
                            object = new Grass(i, j, Sprite.grass.getFxImage());
                            entities.add(new Oneal(i, j, Sprite.oneal_left1.getFxImage()));
                            Entity.countEnemy ++;
                            break;
                        default:
                            object = new Grass(i, j, Sprite.grass.getFxImage());
                            stillObjects.add(object);
                    }
                    stillObjects.add(object);
                }

            }
        }
    }

    public void update() {
        for(int i = 0; i < entities.size(); i++) {
            entities.get(i).update();
        }
        for(int i = 0; i < stillObjects.size(); i++) {
            stillObjects.get(i).update();
        }
    }

    public void render() {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        stillObjects.forEach(g -> g.render(gc));
        entities.forEach(g -> g.render(gc));

    }

    public static Portal getPortal() {
        for(int i = 0; i < stillObjects.size(); i++) {
            if(stillObjects.get(i) instanceof Portal) {
                return (Portal) stillObjects.get(i);
            }
        }
        return null;
    }

    public boolean portalHandle() {
        if((int) Math.round(bomberman.getX()) == (int) BombermanGame.getPortal().getX()
                && (int) Math.round(bomberman.getY()) == (int) BombermanGame.getPortal().getY()) {
            if(BombermanGame.getPortal().isOpen && bomberman.enter) {
                return true;
            }
        }
        return false;
    }

}