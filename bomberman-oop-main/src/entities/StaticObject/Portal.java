package entities.StaticObject;

import javafx.scene.image.Image;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;

public class Portal extends StaticObject {
    public boolean isOpen = false;

    public Portal(double x, double y, Image img) {
        super(x, y, img);
    }

    @Override
    public void update() {
        if(countEnemy == 0){
            isOpen = true;
            try {
                URL url = Paths.get("res/sprites/portal_open.png").toUri().toURL();
                this.img = new Image(String.valueOf(url));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

        }
    }
}
