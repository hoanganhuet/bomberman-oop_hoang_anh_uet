package entities.Bomb;

import entities.Entity;
import javafx.scene.image.Image;

public abstract class Bomb extends Entity {
    public Bomb(int x, int y, Image img) {
        super( x, y, img);
    }

    @Override
    public void update() {

    }


}
